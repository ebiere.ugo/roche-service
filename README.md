# roche-service

##Getting Started
This is the Roche product app

##Prerequisites
You need Java8, Maven to run the app
You need Docker/Docker-Compose to run the docker containers

##Running the App
###To build only
`mvn clean install -DskipTests`

###To start the app locally (on your dev machine):
This starts up an in memory H2 db
`mvn clean install -DskipTests`
`java -jar -Dspring.profiles.active=local ./target/roche-service-0.1.0.jar`

###To start the app in any other environment e.g. UAT environment
This expects you have a mysql db running
update the application-uat.properties to reflect your environment db details 
`mvn clean install -DskipTests`
`java -jar -Dspring.profiles.active=uat ./target/roche-service-0.1.0.jar`

##The API spec
The Swagger API spec is available locally at
http://localhost:8080/roche-service/swagger-ui.html

##Deployment
You can deploy easily in UAT or PRODUCTION using docker containers + docker compose
build project:
mvn clean install -DskipTests
build docker container:
`docker build -t roche-service .`
tag the container as required
`docker tag c183d8d32e63 ebiere/roche-service`
edit docker-compose environent variable in roche-service.env as may be required and run
edit docker-compose as may be required and run as daemon
`docker-compose up &`

##License
This project is licensed under the MIT License - see the LICENSE.md file for details