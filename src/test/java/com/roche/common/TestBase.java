package com.roche.common;

import com.roche.dao.ProductRepository;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class TestBase {

    @Autowired
    protected ProductRepository productRepository;

    @Before
    public void setupDB(){
        productRepository.deleteAllInBatch();
        productRepository.flush();
    }

    @After
    public void clearDB(){
        productRepository.deleteAllInBatch();
    }
}
