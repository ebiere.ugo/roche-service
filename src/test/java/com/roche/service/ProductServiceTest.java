package com.roche.service;

import com.roche.common.TestBase;
import com.roche.dao.ProductRepository;
import com.roche.dto.ProductDto;
import com.roche.dto.ProductRequest;
import com.roche.exception.RocheAppException;
import com.roche.model.Product;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductServiceTest extends TestBase {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductService productService;

    @Test
    public void getProducts() {
        clearDB();
        Product newProduct = saveNewProduct();

        List<ProductDto> products = productService.getProducts();

        assertNotNull(products);
        assertFalse(products.isEmpty());
        assertTrue(products.size() == 1);
        assertThat(newProduct.getName(), is(products.get(0).getName()));
        assertThat(newProduct.getPrice(), is(products.get(0).getPrice()));
        assertNotNull(products.get(0).getId());
        assertNotNull(products.get(0).getCreated());
    }

    @Test
    public void createProduct() {
        ProductRequest productRequest = ProductRequest.builder().name("name").price(100D).build();

        ProductDto product = productService.createProduct(productRequest);

        assertNotNull(product);
        assertThat(product.getName(), is(productRequest.getName()));
        assertThat(product.getPrice(), is(productRequest.getPrice()));
        assertNotNull(product.getId());
        assertNotNull(product.getCreated());
    }

    @Test
    public void findProductById() {
        Product newProduct = saveNewProduct();

        ProductDto productDto = productService.findProductById(newProduct.getId());

        assertNotNull(productDto);
        assertThat(newProduct.getName(), is(productDto.getName()));
        assertThat(newProduct.getPrice(), is(productDto.getPrice()));
        assertThat(newProduct.getId(), is(productDto.getId()));
        assertThat(newProduct.getCreated(), is(productDto.getCreated()));
    }

    @Test(expected = RocheAppException.class)
    public void findProductByIdFail() {
        productService.findProductById(UUID.randomUUID().toString());
    }

    @Test
    public void updateProduct() {
        Product newProduct = saveNewProduct();

        ProductRequest productRequest = ProductRequest.builder().price(200D).name("newName").build();
        ProductDto productDto = productService.updateProduct(newProduct.getId(), productRequest);

        assertNotNull(productDto);
        assertThat(productRequest.getName(), is(productDto.getName()));
        assertThat(productRequest.getPrice(), is(productDto.getPrice()));
    }

    @Test(expected = RocheAppException.class)
    public void updateProductFail() {
        productService.updateProduct(
                UUID.randomUUID().toString(),
                ProductRequest.builder().price(200D).name("newName").build()
        );
    }

    @Test
    public void deleteProduct() {
        Product newProduct = saveNewProduct();
        productService.deleteProduct(newProduct.getId());
        assertTrue(productRepository.findByIdAndDeleted(newProduct.getId(), true).isPresent());
        assertFalse(productRepository.findByIdAndDeleted(newProduct.getId(), false).isPresent());
    }

    @Test(expected = RocheAppException.class)
    public void deleteProductFail() {
        productService.deleteProduct(UUID.randomUUID().toString());
    }

    private Product saveNewProduct() {
        Product product = new Product();
        product.setPrice(100D);
        product.setName("product");
        return productRepository.save(product);
    }
}