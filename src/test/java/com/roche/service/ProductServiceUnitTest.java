package com.roche.service;


import com.roche.dao.ProductRepository;
import com.roche.dto.ProductDto;
import com.roche.dto.ProductRequest;
import com.roche.exception.RocheAppException;
import com.roche.model.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceUnitTest {

    @Mock
    ProductRepository productRepository;

    @InjectMocks
    ProductService productService;

    @Test
    public void getProducts() {
        Product newProduct = getProduct();

        Mockito.when(productRepository.findByDeleted(false))
                .thenReturn(Arrays.asList(newProduct));

        List<ProductDto> products = productService.getProducts();

        assertNotNull(products);
        assertFalse(products.isEmpty());
        assertTrue(products.size() == 1);
        assertThat(newProduct.getName(), is(products.get(0).getName()));
        assertThat(newProduct.getPrice(), is(products.get(0).getPrice()));
        assertNotNull(products.get(0).getId());
        assertNotNull(products.get(0).getCreated());
    }

    @Test
    public void createProduct() {
        Product newProduct = getProduct();

        Mockito.when(productRepository.save(Mockito.any()))
                .thenReturn(newProduct);
        ProductRequest productRequest =
                ProductRequest.builder().name(newProduct.getName()).price(newProduct.getPrice()).build();
        ProductDto product = productService.createProduct(productRequest);

        assertNotNull(product);
        assertThat(product.getName(), is(productRequest.getName()));
        assertThat(product.getPrice(), is(productRequest.getPrice()));
        assertNotNull(product.getId());
        assertNotNull(product.getCreated());
    }

    @Test
    public void findProductById() {
        Product newProduct = getProduct();

        Mockito.when(productRepository.findByIdAndDeleted(newProduct.getId(), false))
                .thenReturn(Optional.of(newProduct));

        ProductDto productDto = productService.findProductById(newProduct.getId());

        assertNotNull(productDto);
        assertThat(newProduct.getName(), is(productDto.getName()));
        assertThat(newProduct.getPrice(), is(productDto.getPrice()));
        assertThat(newProduct.getId(), is(productDto.getId()));
        assertThat(newProduct.getCreated(), is(productDto.getCreated()));
    }

    @Test(expected = RocheAppException.class)
    public void findProductByIdFail() {
//        Mockito.when(productRepository.findByIdAndDeleted(Mockito.anyString(), Mockito.anyBoolean()))
//                .thenReturn(Optional.empty());
        productService.findProductById(UUID.randomUUID().toString());
    }

    @Test
    public void updateProduct() {
        Product newProduct = getProduct();

        Mockito.when(productRepository.findByIdAndDeleted(newProduct.getId(), false))
                .thenReturn(Optional.of(newProduct));

        ProductRequest productRequest = ProductRequest.builder().price(200D).name("newName").build();
        ProductDto productDto = productService.updateProduct(newProduct.getId(), productRequest);

        assertNotNull(productDto);
        assertThat(productRequest.getName(), is(productDto.getName()));
        assertThat(productRequest.getPrice(), is(productDto.getPrice()));
    }

    @Test(expected = RocheAppException.class)
    public void updateProductFail() {
//        Mockito.when(productRepository.findByIdAndDeleted(Mockito.anyString(), Mockito.anyBoolean()))
//                .thenReturn(Optional.empty());
        productService.updateProduct(
                UUID.randomUUID().toString(),
                ProductRequest.builder().price(200D).name("newName").build()
        );
    }

    @Test
    public void deleteProduct() {
        Product newProduct = getProduct();
        Mockito.when(productRepository.findByIdAndDeleted(newProduct.getId(), false))
                .thenReturn(Optional.of(newProduct));
        productService.deleteProduct(newProduct.getId());
    }

    @Test(expected = RocheAppException.class)
    public void deleteProductFail() {
        productService.deleteProduct(UUID.randomUUID().toString());
    }

    private Product getProduct() {
        Product newProduct = new Product();
        newProduct.setId(UUID.randomUUID().toString());
        newProduct.setCreated(OffsetDateTime.now());
        newProduct.setName("name");
        newProduct.setPrice(1000D);
        return newProduct;
    }
}
