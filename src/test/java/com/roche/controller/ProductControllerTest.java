package com.roche.controller;

import com.roche.dao.ProductRepository;
import com.roche.dto.ProductDto;
import com.roche.dto.ProductRequest;
import com.roche.model.Product;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

import static com.roche.service.ProductService.PRODUCT_S_DOES_NOT_EXIST;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductControllerTest {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getProducts() {
        clearDB();
        Product newProduct = saveNewProduct();

        ResponseEntity<List<ProductDto>> result = restTemplate.exchange(
                "/v1/products", HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ProductDto>>() {
                }
        );

        assertTrue(result.getStatusCode() == HttpStatus.OK);
        assertNotNull(result.getBody());
        assertFalse(result.getBody().isEmpty());

        List<ProductDto> products = result.getBody();

        assertThat(products.size(), is(1));
        assertThat(newProduct.getName(), is(products.get(0).getName()));
        assertThat(newProduct.getPrice(), is(products.get(0).getPrice()));
        assertNotNull(products.get(0).getId());
        assertNotNull(products.get(0).getCreated());
    }

    @Test
    public void createProduct() {
        ProductRequest productRequest = ProductRequest.builder().name("name1").price(1000D).build();
        HttpEntity<ProductRequest> requestEntity = new HttpEntity<>(productRequest);
        ResponseEntity<ProductDto> result = restTemplate.exchange(
                "/v1/products",
                HttpMethod.POST,
                requestEntity,
                ProductDto.class
        );

        assertTrue(result.getStatusCode() == HttpStatus.OK);
        assertNotNull(result.getBody());

        ProductDto product = result.getBody();
        assertNotNull(product);
        assertThat(product.getName(), is(productRequest.getName()));
        assertThat(product.getPrice(), is(productRequest.getPrice()));
        assertNotNull(product.getId());
        assertNotNull(product.getCreated());
    }

    @Test
    public void findProductById() {
        Product newProduct = saveNewProduct();

        ResponseEntity<ProductDto> result = restTemplate.exchange(
                "/v1/products/" + newProduct.getId(), HttpMethod.GET,
                null,
                ProductDto.class
        );

        assertTrue(result.getStatusCode() == HttpStatus.OK);
        assertNotNull(result.getBody());

        ProductDto productDto = result.getBody();

        assertNotNull(productDto);
        assertThat(newProduct.getName(), is(productDto.getName()));
        assertThat(newProduct.getPrice(), is(productDto.getPrice()));
        assertThat(newProduct.getId(), is(productDto.getId()));
        assertThat(newProduct.getCreated(), is(productDto.getCreated()));
    }

    @Test
    public void findProductByIdFail() {
        String id = UUID.randomUUID().toString();
        ResponseEntity result = restTemplate.exchange(
                "/v1/products/" + id, HttpMethod.GET,
                null,
                String.class
        );

        assertThat(result.getStatusCode(), is(HttpStatus.NOT_FOUND));
        assertThat(result.getBody(), is(String.format(PRODUCT_S_DOES_NOT_EXIST, id)));
    }


    @Test
    public void updateProduct() {
        Product newProduct = saveNewProduct();

        ProductRequest productRequest = ProductRequest.builder().price(900D).name("newName9").build();
        HttpEntity<ProductRequest> requestEntity = new HttpEntity<>(productRequest);
        ResponseEntity<ProductDto> result = restTemplate.exchange(
                "/v1/products/" + newProduct.getId(), HttpMethod.PUT,
                requestEntity,
                ProductDto.class
        );

        assertTrue(result.getStatusCode() == HttpStatus.OK);
        assertNotNull(result.getBody());
        ProductDto productDto = result.getBody();

        assertNotNull(productDto);
        assertThat(productRequest.getName(), is(productDto.getName()));
        assertThat(productRequest.getPrice(), is(productDto.getPrice()));
    }

    @Test
    public void updateProductFail() {
        String id = UUID.randomUUID().toString();
        HttpEntity<ProductRequest> requestEntity = new HttpEntity<>(
                ProductRequest.builder().price(900D).name("newName9").build()
        );
        ResponseEntity<String> result = restTemplate.exchange(
                "/v1/products/" + id, HttpMethod.PUT,
                requestEntity,
                String.class
        );

        assertThat(result.getStatusCode(), is(HttpStatus.NOT_FOUND));
        assertThat(result.getBody(), is(String.format(PRODUCT_S_DOES_NOT_EXIST, id)));
    }

    @Test
    public void deleteProduct() {
        Product newProduct = saveNewProduct();
        //restTemplate.delete("/v1/products/" + newProduct.getId());
        ResponseEntity<String> result = restTemplate.exchange(
                "/v1/products/" + newProduct.getId(), HttpMethod.DELETE,
                null,
                String.class
        );

        assertThat(result.getStatusCode(), is(HttpStatus.OK));

        assertTrue(productRepository.findByIdAndDeleted(newProduct.getId(), true).isPresent());
        assertFalse(productRepository.findByIdAndDeleted(newProduct.getId(), false).isPresent());
    }

    @Test
    public void deleteProductFail() {
        String id = UUID.randomUUID().toString();
        ResponseEntity<String> result = restTemplate.exchange(
                "/v1/products/" + id, HttpMethod.DELETE,
                null,
                String.class
        );

        assertThat(result.getStatusCode(), is(HttpStatus.NOT_FOUND));
        assertThat(result.getBody(), is(String.format(PRODUCT_S_DOES_NOT_EXIST, id)));
    }

    private Product saveNewProduct() {
        Product product = new Product();
        product.setPrice(100D);
        product.setName("product");
        return productRepository.save(product);
    }

    @Before
    public void setupDB() {
        productRepository.deleteAllInBatch();
        productRepository.flush();
    }

    @After
    public void clearDB() {
        productRepository.deleteAllInBatch();
    }
}