package com.roche.dao;

import com.roche.common.TestBase;
import com.roche.dao.ProductRepository;
import com.roche.dto.ProductDto;
import com.roche.dto.ProductRequest;
import com.roche.exception.RocheAppException;
import com.roche.model.Product;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductRepositoryTest extends TestBase {

    @Test
    public void findByIdAndDeleted() {
        Product product = new Product();
        product.setPrice(100D);
        product.setName("product");
        product.setDeleted(false);
        Product savedProduct = productRepository.save(product);
        assertTrue(productRepository.findByIdAndDeleted(savedProduct.getId(), false).isPresent());
        assertFalse(productRepository.findByIdAndDeleted(savedProduct.getId(), true).isPresent());
    }

    @Test
    public void findByDeleted() {
        Product product = new Product();
        product.setPrice(100D);
        product.setName("product");
        product.setDeleted(false);
        productRepository.save(product);
        assertFalse(productRepository.findByDeleted(false).isEmpty());
        assertTrue(productRepository.findByDeleted(true).isEmpty());
    }
}