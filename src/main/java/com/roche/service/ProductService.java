package com.roche.service;


import com.roche.dao.ProductRepository;
import com.roche.dto.ProductRequest;
import com.roche.dto.ProductDto;
import com.roche.exception.RocheAppException;
import com.roche.model.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProductService {
    public static final String PRODUCT_S_DOES_NOT_EXIST = "Product %s  does not exist!!";

    @Autowired
    ProductRepository productRepository;

    public List<ProductDto> getProducts() {
        return productRepository.findByDeleted(false).stream()
        .map(this::convertToProductDto)
        .collect(Collectors.toList());
    }

    private ProductDto convertToProductDto(Product product) {
        ProductDto productDto = new ProductDto();
        BeanUtils.copyProperties(product, productDto);
        return productDto;
    }

    public ProductDto createProduct(ProductRequest productRequest) {
        Product product = new Product();
        product.setName(productRequest.getName());
        product.setPrice(productRequest.getPrice());
        return convertToProductDto(productRepository.save(product));
    }

    public ProductDto findProductById(String id) {
        Optional<Product> productExists = productRepository.findByIdAndDeleted(id, false);
        if(!productExists.isPresent()){
            log.error(String.format(PRODUCT_S_DOES_NOT_EXIST, id));
            throw new RocheAppException(String.format(PRODUCT_S_DOES_NOT_EXIST, id), HttpStatus.NOT_FOUND);
        }

        return convertToProductDto(productExists.get());
    }

    @Transactional
    public ProductDto updateProduct(String id, ProductRequest productRequest) {
        Optional<Product> productExists = productRepository.findByIdAndDeleted(id, false);
        if(!productExists.isPresent()){
            log.error(String.format(PRODUCT_S_DOES_NOT_EXIST, id));
            throw new RocheAppException(String.format(PRODUCT_S_DOES_NOT_EXIST, id), HttpStatus.NOT_FOUND);
        }

        productExists.get().setName(productRequest.getName());
        productExists.get().setPrice(productRequest.getPrice());

        return convertToProductDto(productExists.get());
    }


    @Transactional
    public void deleteProduct(String id) {
        Optional<Product> productExists = productRepository.findByIdAndDeleted(id, false);
        if(!productExists.isPresent()){
            log.error(String.format(PRODUCT_S_DOES_NOT_EXIST, id));
            throw new RocheAppException(String.format(PRODUCT_S_DOES_NOT_EXIST, id), HttpStatus.NOT_FOUND);
        }

        productExists.get().setDeleted(Boolean.TRUE);
    }
}
