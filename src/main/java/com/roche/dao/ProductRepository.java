package com.roche.dao;

import com.roche.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface ProductRepository extends JpaRepository<Product, String> {

    Optional<Product> findByIdAndDeleted(String id, boolean deleted);

    List<Product> findByDeleted(boolean deleted);
}
