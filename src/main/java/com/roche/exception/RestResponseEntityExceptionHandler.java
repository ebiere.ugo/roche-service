package com.roche.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler
        extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {RocheAppException.class})
    protected ResponseEntity<Object> handleAppException(RuntimeException ex, WebRequest request) {
        RocheAppException appException = (RocheAppException) ex;
        return handleExceptionInternal(
                ex,
                appException.getMessage(),
                new HttpHeaders(),
                appException.getHttpStatus(),
                request
        );
    }
}
