package com.roche.exception;

import org.springframework.http.HttpStatus;

public class RocheAppException extends RuntimeException {
    private HttpStatus httpStatus;

    public RocheAppException(String msg, HttpStatus httpStatus){
        super(msg);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus(){
        return httpStatus;
    }
}
